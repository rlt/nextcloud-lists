# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).


## [1.0.1] - 2021-10-27

### Fixed

- Creation of new checklists now works

## [1.0.0] - 2021-10-27

### Added

- Official release
- Frontend is now a real checklist and not text

### Fixed

- Data return from frontend wasn't trim from whitespace, causing the app to
  crash
- 

## [0.0.3] - 2021-10-22

### Fixed

- New content is return from backend when updated. Reload in frontend happen.
- Updating the title does not forget the .md extension
- essentially : all operations on list now work

## [0.0.2] - 2021-10-22

### Added

- First release
- You can now create, modify and delete checklist
- Text interface

## [0.0.1] - 2021-10-08

### Added

- Transforming Notestutorial into a clean template for this app
