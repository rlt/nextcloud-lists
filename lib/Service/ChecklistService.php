<?php

namespace OCA\Lists\Service;

use Exception;

use OCP\Files\NotPermittedException;
use OCP\Files\NotFoundException;
use OCP\Files\InvalidPathException;
use OCP\Files\Folder;
use OCP\Files\File;
use OCP\Files\IRootFolder;


class ChecklistService {

    /** @var IRootStorage */
    private $storage;

    public function __construct(IRootFolder $storage){
        $this->storage = $storage;
    }

	private function handleException(Exception $e): void {
		if ($e instanceof NotPermittedException ||
			$e instanceof NotFoundException ||
			$e instanceof InvalidPathException) {
			throw new ChecklistNotFoundException($e->getMessage());
		} else {
			throw $e;
		}
	}

	private function getOrCreateFolder(string $path) : Folder {
		if ($this->storage->nodeExists($path)) {
			$folder = $this->storage->get($path);
		} else {
			$folder = $this->storage->newFolder($path);
		}
		if (!($folder instanceof Folder)) {
			throw new ChecklistFolderException($path.' is not a folder');
		}
		return $folder;
	}

	private function makeObjectFromFile (File $file) {
		$stream = $file->fopen('r');
		$title = "";
		$content = array();

		// deal with first line (potential title)
		if(!feof($stream)) {
			$line = fgets($stream);
			if ($line[0] === "#") {
				$title = preg_replace('/^# /', '', $line);
			} else {
				$title = $file->getName();
				$title = preg_replace('/\.md$/', '', $title);
				
				$line = preg_filter('/^[\*-] \[(x| )\] /', '\1', $line);
				if ($line != null) {
					$checkitem = new Checkitem();
					switch ($line[0]) {
						case " ":
							$checkitem->setIsCheck(false);
							break;
						case "x":
							$checkitem->setIsCheck(true);
							break;
					}
					$checkitem->setTextfield(substr($line,1));
					$content[] = $checkitem;
				}
			}
		}

		// deal with all the other lines
		while(!feof($stream)) {
			$line = fgets($stream);
			$line = preg_filter('/^[\*-] \[(x| )\] /', '\1', $line);
			if ($line != null) {
				$checkitem = new Checkitem();
				switch ($line[0]) {
					case " ":
						$checkitem->setIsCheck(false);
						break;
					case "x":
						$checkitem->setIsCheck(true);
						break;
				}
				$checkitem->setTextfield(substr($line,1));
				$content[] = $checkitem;
			}
			
		}
		fclose($stream);

		$checklist = new Checklist();
		$checklist->setId($file->getId());
		$checklist->setTitle($title);
		$checklist->setContent($content);

		return $checklist;
	}

	private function writeFileFromObject (Checklist $checklist, Folder $checklistFolder) {
		if (empty($checklist->getId())) {
			$file = $checklistFolder->newFile($checklist->getTitle() . ".md");
			$checklist->setId($file->getId());
		} else {
			$files = $checklistFolder->getById($checklist->getId());
			if (empty($files)) {
				$file = $checklistFolder->newFile($checklist->getTitle() . ".md");
				$checklist->setId($file->getId());
			} else {
				$file = $files[0];
			}
		}
	
		if (!($file instanceof File)) {
			throw new ChecklistNotFoundException($id.' is not a file');
		}
		
		$title = $checklist->getTitle();
		$filename = preg_replace('/\.md$/', '', $file->getName());
		if ($filename !== $title) {
			$file = $file->move($checklistFolder->getPath() . "/" . $title . ".md");
		}

		$file->putContent($checklist->toString());

		return $checklist;
	}

	public function findAll(string $userId): array {
		$checklistFolder = $this->getOrCreateFolder('/' . $userId . '/files/Checklist');
		$files = $checklistFolder->getDirectoryListing();
		$arr = array();
		foreach ($files as $file) {
			if (($file instanceof File)) {
				$arr[] = $this->makeObjectFromFile($file);
			}
		}
		return $arr;
	}

	public function find($id, $userId) {
		$checklistFolder = $this->getOrCreateFolder('/' . $userId . '/files/Checklist');
		$file = $checklistFolder->getById($id)[0];
		return $this.makeObjectFromFile($file);
	}

	public function create($title, $content, $userId) {
		$checklistFolder = $this->getOrCreateFolder('/' . $userId . '/files/Checklist');
		$checklist = new Checklist ();
		$checklist->setTitle($title);
		foreach ($content as $checkitemArray) {
			$checkitem = new Checkitem;
			$checkitem->setIsCheck($checkitemArray["isCheck"]);
			$checkitem->setTextfield($checkitemArray["textfield"]);
			$checklist->addCheckitem($checkitem);
		}
		$checklist->setUserId($userId);

		return $this->writeFileFromObject($checklist, $checklistFolder);
	}

	public function update($id, $title, $content, $userId) {
		$checklistFolder = $this->getOrCreateFolder('/' . $userId . '/files/Checklist');
		$checklist = new Checklist ();
		$checklist->setTitle($title);
		foreach ($content as $checkitemArray) {
			$checkitem = new Checkitem;
			$checkitem->setIsCheck($checkitemArray["isCheck"]);
			$checkitem->setTextfield($checkitemArray["textfield"]);
			$checklist->addCheckitem($checkitem);
		}
		$checklist->setUserId($userId);
		// only difference with create is setId()
		$checklist->setId($id);

		return $this->writeFileFromObject($checklist, $checklistFolder);
	}

	public function delete($id, $userId) {
		$checklistFolder = $this->getOrCreateFolder('/' . $userId . '/files/Checklist');

		$file = $checklistFolder->getById($id)[0];
		try {
			$file->delete();
		} catch (\Throwable $e) {
			handleException($e);
		}
	}
}
