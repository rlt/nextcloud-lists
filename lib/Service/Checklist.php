<?php

namespace OCA\Lists\Service;

use JsonSerializable;


class Checklist implements JsonSerializable {
	protected $id; // the backing file id
	protected $title; // first line / filename
	protected $content; // array of Checkitem
	protected $userId;

	public function jsonSerialize(): array {
		return [
			'id' => $this->id,
			'title' => $this->title,
			'content' => $this->content
		];
	}

	public function getId() {
		return $this->id;
	}

	public function getTitle() {
		return $this->title;
	}

	public function getUserId() {
		return $this->userId;
	}

	public function getContent() {
		return $this->content;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function setTitle($title) {
		$this->title = trim($title);
	}

	public function setContent($content) {
		$this->content = $content;
	}

	public function setUserId($userId) {
		$this->userId = $userId;
	}

	public function addCheckitem($checkitem) {
		$this->content[] = $checkitem;
	}

	public function toString() {
		$contentStr = "# " . $this->title . "\n";
		foreach ($this->content as $checkitem) {
			$contentStr .= $checkitem->toString() . "\n"; //pblm ici
		}
		return $contentStr;
	}
}
