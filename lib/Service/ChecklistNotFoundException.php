<?php

declare(strict_types=1);

namespace OCA\Lists\Service;

use Exception;

class ChecklistNotFoundException extends Exception {
}
