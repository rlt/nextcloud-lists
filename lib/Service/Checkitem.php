<?php

namespace OCA\Lists\Service;

use JsonSerializable;

class checkitem implements JsonSerializable {
	protected $isCheck;
	protected $textfield;

	public function jsonSerialize(): array {
		return [
			'isCheck' => $this->isCheck,
			'textfield' => $this->textfield
		];
	}

	public function getIsCheck() {
		return $this->isCheck;
	}

	public function getTextfield() {
		return $this->textfield;
	}

	public function setIsCheck($isCheck) {
		$this->isCheck = $isCheck;
	}

	public function setTextfield($textfield) {
		$this->textfield = trim($textfield);
	}

	public function toString() {
		if ($this->isCheck) {
			return "- [x] " . $this->textfield;
		}
		return "- [ ] " . $this->textfield;
	}
}
