<?php

namespace OCA\Lists\AppInfo;

use OCP\AppFramework\App;

class Application extends App {
	public const APP_ID = 'lists';

	public function __construct() {
		parent::__construct(self::APP_ID);
	}
}
