<?php

return [
	'resources' => [
		'checklist' => ['url' => '/checklists'],
	],
	'routes' => [
		['name' => 'page#index', 'url' => '/', 'verb' => 'GET'],
	]
];
